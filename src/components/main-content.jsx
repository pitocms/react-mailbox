import React, {Component} from 'react';
import Breadcrumb from './contents/breadcrumb';
import LeftSidebar from './contents/left-sidebar';
import Inbox from './contents/inbox';
import Compose from './contents/compose';
import Footer from './footer';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

class MainContent extends Component{
	render(){
		return(
		
				<div className="content-wrapper ml-0 ">
	                
	                <Router>
				    < Breadcrumb />
				    
				    <section className="content mb-4">
				      
				        <div className="row">
					          < LeftSidebar />
					          <Switch>
					            <Route path="/"        exact component={Inbox} />
			                  	<Route path="/inbox"   exact component={Inbox} />
			                  	<Route path="/sent"    exact component={Inbox} />
			                  	<Route path="/drafts"  exact component={Inbox} />
			                  	<Route path="/junk"    exact component={Inbox} />
			                  	<Route path="/trash"   exact component={Inbox} />
			                  	<Route path="/compose" exact component={Compose} />
			                  </Switch>   
					    </div>
					    
				    </section >

				    </Router>

				    <Footer />

			    </div>

		)
	}
}

export default MainContent;
