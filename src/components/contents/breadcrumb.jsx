import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

class BreadCrumb extends Component{

	render(){
		return(

			<section className="content-header">
		      <div className="container-fluid">
		        <div className="row mb-2">
		          <div className="col-sm-6">
		           
			        <Switch>
				        <Route path="/:id" children={<Child />} />
				    </Switch>
			        
		          </div>
		          <div className="col-sm-6">
		            <ol className="breadcrumb float-sm-right">
		              <li className="breadcrumb-item"><a href="#">Home</a></li>
		              <li className="breadcrumb-item active">Inbox</li>
		            </ol>
		          </div>
		        </div>
		      </div>
		    </section>

		)
	}
}

export default BreadCrumb;

function Child() {
  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.
  let { id } = useParams();
  
  return (
    <div>
       <h1 className="text-uppercase">{id}</h1>
    </div>
  );
}