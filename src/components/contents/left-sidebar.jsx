import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class LeftSidebar extends Component{
	render(){
		return(
			<div className="col-md-3">

			  <Link to="/compose" className="btn btn-primary btn-block mb-3">
			  		Compose
			  </Link>
	     

	          <div className="card">
	            <div className="card-header">
	              <h3 className="card-title">Folders</h3>

	              <div className="card-tools">
	                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i>
	                </button>
	              </div>
	            </div>
	            <div className="card-body p-0">
	              <ul className="nav nav-pills flex-column">

	                <li className="nav-item active">
	                  <Link to="/inbox" className="nav-link">
	                    <i className="fas fa-inbox"></i> Inbox
	                    <span className="badge bg-primary float-right">12</span>
	                  </Link>
	                </li>

	                
	                <li className="nav-item">
	                  <Link to="/sent" className="nav-link">
	                    <i className="far fa-envelope"></i> Sent
	                  </Link>
	                </li>

	                <li className="nav-item">
	                  <Link to="/drafts" className="nav-link">
	                    <i className="far fa-envelope"></i> Drafts
	                  </Link>
	                </li>

	                <li className="nav-item active">
	                  <Link to="/junk" className="nav-link">
	                    <i className="fas fa-filter"></i> Junk
	                    <span className="badge bg-warning float-right">65</span>
	                  </Link>
	                </li>

	                <li className="nav-item">
	                  <Link to="/trash" className="nav-link">
	                    <i className="far fa-envelope"></i> Trash
	                  </Link>
	                </li>
	              </ul>
	            </div>
	         
	          </div>
	         
	          <div className="card">
	            <div className="card-header">
	              <h3 className="card-title">Labels</h3>

	              <div className="card-tools">
	                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i>
	                </button>
	              </div>
	            </div>
	            <div className="card-body p-0">
	              <ul className="nav nav-pills flex-column">
	                <li className="nav-item">
	                  <a href="#" className="nav-link">
	                    <i className="far fa-circle text-danger"></i>
	                    Important
	                  </a>
	                </li>
	                <li className="nav-item">
	                  <a href="#" className="nav-link">
	                    <i className="far fa-circle text-warning"></i> Promotions
	                  </a>
	                </li>
	                <li className="nav-item">
	                  <a href="#" className="nav-link">
	                    <i className="far fa-circle text-primary"></i>
	                    Social
	                  </a>
	                </li>
	              </ul>
	            </div>
	           
	          </div>
	          
	        </div>
		)
	}
}

export default LeftSidebar;