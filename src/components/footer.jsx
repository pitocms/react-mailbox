import React, {Component} from 'react';

class Footer extends Component{
	render(){
		return(
			<footer className="main-footer ml-0">
			    <div className="float-right d-none d-sm-block">
			      <b>Version</b> 1.0.0
			    </div>
			    <strong>Copyright &copy; 2020 <a href="#">Admin</a>.</strong> All rights
			    reserved.
			</footer>
		)
	}
}

export default Footer;