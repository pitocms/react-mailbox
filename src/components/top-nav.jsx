import React from 'react';


class Nav extends React.Component{
     render(){
        return (
    		<nav className="main-header navbar navbar-expand navbar-white navbar-light ml-0">
  
			    <ul className="navbar-nav">
			  
			      <li className="nav-item d-none d-sm-inline-block">
			        <a href="../../index3.html" className="nav-link">Home</a>
			      </li>
			      <li className="nav-item d-none d-sm-inline-block">
			        <a href="#" className="nav-link">Contact</a>
			      </li>
			    </ul>

	  
			    <form className="form-inline ml-3">
			      <div className="input-group input-group-sm">
			        <input className="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
			        <div className="input-group-append">
			          <button className="btn btn-navbar" type="submit">
			            <i className="fas fa-search"></i>
			          </button>
			        </div>
			      </div>
			    </form>

	  
			    <ul className="navbar-nav ml-auto">
			    
			      <li className="nav-item dropdown">
			        <a className="nav-link" href="#">
			          Logout
			        </a>

			      </li>
			    </ul>
		  </nav>
        )
     }
}


export default Nav;
