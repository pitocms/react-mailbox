import React from 'react';
import Nav from './top-nav';
import MainContent from './main-content';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/dist/css/adminlte.min.css';
import '../assets/plugins/fontawesome-free/css/all.min.css';


export default class Main extends React.Component{
     render(){

     	return(
     		<div>
	     		< Nav />
	     		< MainContent />
     		</div>
     	)
     	
     }
}