import React from 'react';
import ReactDOM from 'react-dom';
import MainLayout from './components/main-layout';

ReactDOM.render(<MainLayout />,document.getElementById('root'));